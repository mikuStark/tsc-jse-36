package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectServiceTest {

    @Nullable
    private ProjectService projectService;
    @Nullable
    private Project project;
    private String userLogin = "test";

    @Before
    public void before() {
        projectService = new ProjectService(new ConnectionService(new PropertyService()));
        projectService.add(userLogin, new Project("Project"));
        project = projectService.findByIndex(userLogin, 0);
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NonNull final Project projectById = projectService.findById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void findAll() {
        @NonNull final List<Project> projects = projectService.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NonNull final List<Project> projects = projectService.findAll(userLogin);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByErrorUserId() {
        @NonNull final List<Project> projects = projectService.findAll("ertyut");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findAllByName() {
        @NonNull final Project projects = projectService.findByName(userLogin, project.getName());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findAllByErrorName() {
        @NonNull final Project projects = projectService.findByName(userLogin, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    public void removeById() {
        projectService.remove(userLogin, project);
        Assert.assertNull(projectService.findById(userLogin, project.getId()));
    }

    @Test
    public void removeByErrorUserId() {
        projectService.removeById("sd", project.getId());
        @NonNull final List<Project> projects = projectService.findAll();
        Assert.assertEquals(0, projects.size());
    }
}
