package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.FieldConst;
import ru.tsc.karbainova.tm.api.TableConst;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.model.Project;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NonNull final Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.PROJECT_TABLE;
    }

    @SneakyThrows
    @Override
    protected Project fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NonNull final Project project = new Project();
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setId(row.getString(FieldConst.ID));
        project.setUserId(row.getString(FieldConst.USER_ID));
        project.setStartDate(row.getDate(FieldConst.START_DATE));
        project.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        project.setCreated(row.getDate(FieldConst.CREATED));
        return project;
    }

    @Override
    @SneakyThrows
    public Project add(Project entity) {
        if (entity == null) return null;
        @NonNull final String query = "INSERT INTO " + getTableName() +
                "(id, name, description, status, start_date, finish_date, created, user_id)" +
                "VALUES(?,?,?,?,?,?,?,?)";
        @NonNull PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public Project update(Project entity) {
        if (entity == null) return null;
        @NonNull final String query = "UPDATE " + getTableName() +
                "SET name=?, description=?, status=?, start_date=?, finish_date=?, created=?, user_id=?)";
        @NonNull PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    @SneakyThrows
    public Project findByName(@NonNull String userId, @NonNull String name) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ? LIMIT 1";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NonNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public Project findByIndex(@NonNull String userId, @NonNull int index) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? LIMIT 1";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NonNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeByName(@NonNull String userId, @NonNull String name) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE name = ? AND user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeByIndex(String userId, int index) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }
}
