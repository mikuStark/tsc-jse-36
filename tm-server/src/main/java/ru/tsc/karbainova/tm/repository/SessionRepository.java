package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.FieldConst;
import ru.tsc.karbainova.tm.api.TableConst;
import ru.tsc.karbainova.tm.api.repository.ISessionRepository;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NonNull final Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.SESSION_TABLE;
    }

    @SneakyThrows
    @Override
    protected Session fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NonNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID));
        session.setUserId(row.getString(FieldConst.USER_ID));
        session.setSignature(row.getString(FieldConst.SIGNSTURE));
        session.setTimestamp(row.getLong(FieldConst.TIMESTAMP));
        return session;
    }

    @Override
    @SneakyThrows
    public Session add(Session entity) {
        if (entity == null) return null;
        @NonNull final String query = "INSERT INTO " + getTableName() +
                "(id, user_id, signature, timestamp)" +
                "VALUES(?,?,?,?)";
        @NonNull PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getUserId());
        statement.setString(3, entity.getSignature());
        statement.setString(4, entity.getTimestamp().toString());
        statement.executeUpdate();
        statement.close();
        return entity;
    }
}
