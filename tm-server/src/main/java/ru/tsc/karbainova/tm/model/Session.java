package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.Setter;

public final class Session extends AbstractEntity implements Cloneable {

    public Session() {
    }

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Getter
    @Setter
    private Long timestamp;

    @Getter
    @Setter
    private String userId;

    @Getter
    @Setter
    private String signature;


}
