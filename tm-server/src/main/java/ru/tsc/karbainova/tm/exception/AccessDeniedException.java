package ru.tsc.karbainova.tm.exception;

public class AccessDeniedException extends AbstractException {
    public AccessDeniedException() {
        super("Нет доступа");
    }
}
