package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.ISessionRepository;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.ISessionService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.component.Bootstrap;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.AccessDeniedException;
import ru.tsc.karbainova.tm.exception.AccessForbiddenException;
import ru.tsc.karbainova.tm.exception.empty.*;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.repository.SessionRepository;
import ru.tsc.karbainova.tm.repository.TaskRepository;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;
import java.util.stream.Collectors;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(IConnectionService connectionService) {
        super(connectionService);
    }

    public ISessionRepository getRepository(@NonNull Connection connection) {
        return new SessionRepository(connection);
    }

    @Override
    public boolean checkDataAccess(String login, String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        ServiceLocator serviceLocator = new Bootstrap();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new EmptyUserNotFoundException();
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    public Session open(String login, String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new EmptyLoginOrPasswordException();
//        if (check) throw new EmptyLoginOrPasswordException();
        ServiceLocator serviceLocator = new Bootstrap();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new EmptyLoginException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(session);
            connection.commit();
            return sign(session);
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public Session sign(Session session) {
        if (session == null) throw new EmptySessionNlException();
        session.setSignature(null);
        IPropertyService propertyService = new PropertyService();
        final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    public List<Session> getListSessionByUserId(String userId) {
        return findAll()
                .stream()
                .filter(s -> s.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public void validate(Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NonNull final String signatureSource = session.getSignature();
        @NonNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ISessionRepository sessionRepository = getRepository(connection);
            if (!sessionRepository.exists(session.getId())) throw new AccessDeniedException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void validate(Session session, Role role) {
        if (role == null) throw new AccessForbiddenException();
        validate(session);
        final String userId = session.getUserId();
        ServiceLocator serviceLocator = new Bootstrap();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessForbiddenException();
        if (user.getRole() == null) throw new AccessForbiddenException();
        if (!role.equals(user.getRole())) throw new AccessForbiddenException();
    }

    @Override
    @SneakyThrows
    public void close(Session session) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.remove(session);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void closeAll(Session session) {
        validate(session);
        List<Session> sessions = findAll().stream().filter(s -> s.getUserId().equals(session.getUserId())).collect(Collectors.toList());
        sessions.forEach(this::close);
    }

    @Override
    public void signOutByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return;
        List<Session> sessions = findAll().stream().filter(s -> s.getUserId().equals(userId)).collect(Collectors.toList());
        sessions.forEach(this::close);
    }
}
