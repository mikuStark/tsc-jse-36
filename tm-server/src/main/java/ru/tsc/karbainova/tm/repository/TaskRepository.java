package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.FieldConst;
import ru.tsc.karbainova.tm.api.TableConst;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(@NonNull final Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.TASK_TABLE;
    }

    @SneakyThrows
    @Override
    protected Task fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NonNull final Task task = new Task();
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setId(row.getString(FieldConst.ID));
        task.setUserId(row.getString(FieldConst.USER_ID));
        task.setStartDate(row.getDate(FieldConst.START_DATE));
        task.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        task.setCreated(row.getDate(FieldConst.CREATED));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID));
        return task;
    }

    @Override
    @SneakyThrows
    public Task add(Task entity) {
        if (entity == null) return null;
        @NonNull final String query = "INSERT INTO " + getTableName() +
                "(id, name, description, status, start_date, finish_date, created, user_id, project_id)" +
                "VALUES(?,?,?,?,?,?,?,?,?)";
        @NonNull PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.setString(9, entity.getProjectId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public Task update(Task entity) {
        if (entity == null) return null;
        @NonNull final String query = "UPDATE " + getTableName() +
                "SET name=?, description=?, status=?, start_date=?, finish_date=?, created=?, user_id=?)";
        @NonNull PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    @SneakyThrows
    public Task findByIndex(@NonNull String userId, @NonNull int index) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? LIMIT 1";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NonNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public Task findByName(@NonNull String userId, @NonNull String name) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ? LIMIT 1";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(2, name);
        statement.setString(1, userId);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NonNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeByName(@NonNull String userId, @NonNull String name) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE name = ? AND user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NonNull String userId, @NonNull int index) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public Task taskUnbindById(@NonNull String userId, @NonNull String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(@NonNull String userId, @NonNull String projectId) {
        findAllTaskByProjectId(userId, projectId).forEach(this::remove);
    }

    @Override
    public Task bindTaskToProjectById(@NonNull String userId, @NonNull String projectId, @NonNull String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(@NonNull String userId, @NonNull String projectId) {
        return findAll(userId).stream().filter(o -> userId.equals(o.getUserId()) && projectId.equals(o.getProjectId()))
                .collect(Collectors.toList());
    }
}
