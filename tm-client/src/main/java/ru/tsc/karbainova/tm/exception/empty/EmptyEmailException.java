package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {
    public EmptyEmailException() {
        super("Error Email.");
    }

    public EmptyEmailException(String value) {
        super("Error Email. " + value);
    }
}
