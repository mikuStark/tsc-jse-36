package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.Session;

public class AuthRegistryCommand extends AbstractCommand {
    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Registry app";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter email");
        final String email = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        Session session = serviceLocator.getSession();
        serviceLocator.getAuthEndpoint().registryAuth(session, login, password, email);
    }
}
