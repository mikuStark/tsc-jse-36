package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {
    @NonNull
    private static final String APP_VERSION_DEFAULT = "";
    @NonNull
    private static final String APP_VERSION_KEY = "version";
    @NonNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";
    @NonNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";
    @NonNull
    private static final String DEVELOPER_NAME_KEY = "developer";
    @NonNull
    private static final String DEVELOPER_NAME_DEFAULT = "";
    @NonNull
    private static final String FILE_NAME = "application.properties";
    @NonNull
    private final Properties properties = new Properties();


    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    private String getValue(String name, String defaultName) {
        @Nullable final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getProperty(name);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(name, defaultName);
    }

    @NonNull
    @Override
    public String getApplicationVersion() {
        return getValue(APP_VERSION_KEY, APP_VERSION_DEFAULT);
    }

    @NonNull
    @Override
    public String getDeveloperName() {
        return getValue(DEVELOPER_NAME_KEY, DEVELOPER_NAME_DEFAULT);
    }

    @NonNull
    @Override
    public String getDeveloperEmail() {
        return getValue(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

}
